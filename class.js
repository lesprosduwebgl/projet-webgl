/**
 * Classe sphère, servant à représenter une planète
 * @param  {float} radius      Sert à déterminer le rayon de la planète
 * @param  {String} textureName Nom de la texture à utiliser. Le fichier doit être de type ".png" et être placé dans un sous-dossier textures
 * @param  {float} orbit       Diamètre de l'orbit de la planète. La vitesse de révolution en dépends.
 * @param  {float} tilt        Inclinaison de la planète en radian/pi. 2 = 360degrés.
 * @return {void}
 */
var sphere = function(radius, textureName, orbit, tilt){
    //Déclaration des variables.

     this.radius = radius;
     this.VertexPositionBuffer;
     this.VertexNormalBuffer;
     this.VertexTextureCoordBuffer;
     this.VertexIndexBuffer;
     this.vertexPositionData = [];
     this.normalData = [];
     this.textureCoordData = [];
     this.textureName=textureName;
     this.texture;
     this.orbit = orbit;
     this.orbrot;

     //variable optionnel
     this.tilt = (typeof tilt === "undefined") ? 0 : tilt;


     if (this.orbit==0) {
        this.orbrot = 0.01;
     } else {
        this.orbrot = orbit;
     }

     //Déclaration globale du nom de texture;
     window[textureName];


     /**
      * Création du buffer de la planète
      * @return {void}
      */
     this.buffer= function(){
        var latitudeBands = 30;
        var longitudeBands = 30;

         

        for (var latNumber=0; latNumber <= latitudeBands; latNumber++) {
            var theta = latNumber * Math.PI / latitudeBands;
            var sinTheta = Math.sin(theta);
            var cosTheta = Math.cos(theta);

            for (var longNumber=0; longNumber <= longitudeBands; longNumber++) {
                var phi = longNumber * 2 * Math.PI / longitudeBands;
                var sinPhi = Math.sin(phi);
                var cosPhi = Math.cos(phi);

                var x = cosPhi * sinTheta;
                var y = cosTheta;
                var z = sinPhi * sinTheta;
                var u = 1 - (longNumber / longitudeBands);
                var v = 1 - (latNumber / latitudeBands);

                this.normalData.push(x);
                this.normalData.push(y);
                this.normalData.push(z);
                this.textureCoordData.push(u);
                this.textureCoordData.push(v);
                this.vertexPositionData.push(this.radius * x);
                this.vertexPositionData.push(this.radius * y);
                this.vertexPositionData.push(this.radius * z);
            }
        }

        var indexData = [];
        for (var latNumber=0; latNumber < latitudeBands; latNumber++) {
            for (var longNumber=0; longNumber < longitudeBands; longNumber++) {
                var first = (latNumber * (longitudeBands + 1)) + longNumber;
                var second = first + longitudeBands + 1;
                indexData.push(first);
                indexData.push(second);
                indexData.push(first + 1);

                indexData.push(second);
                indexData.push(second + 1);
                indexData.push(first + 1);
            }
        }



this.VertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.normalData), gl.STATIC_DRAW);
        this.VertexNormalBuffer.itemSize = 3;
        this.VertexNormalBuffer.numItems = this.normalData.length / 3;

        this.VertexTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.textureCoordData), gl.STATIC_DRAW);
        this.VertexTextureCoordBuffer.itemSize = 2;
        this.VertexTextureCoordBuffer.numItems = this.textureCoordData.length / 2;

        this.VertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vertexPositionData), gl.STATIC_DRAW);
        this.VertexPositionBuffer.itemSize = 3;
        this.VertexPositionBuffer.numItems = this.vertexPositionData.length / 3;

        this.VertexIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexData), gl.STATIC_DRAW);
        this.VertexIndexBuffer.itemSize = 1;
        this.VertexIndexBuffer.numItems = indexData.length;
        }


/**
 * Effectue le binding des buffer de la planète
 * @return {void}
 */
this.bind = function(){
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, window[textureName]);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        }

/**
 * Dessine les éléments dans le buffer
 * @return {void}
 */
this.draw = function(){
        gl.drawElements(gl.TRIANGLES, this.VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

        }

/**
 * Initialise la texture de la planète
 * @return {void}
 */
this.createTexture = function(){
        window[textureName] = gl.createTexture();
                window[textureName].image = new Image();
                window[textureName].image.onload = function () {
                    handleLoadedTexture(window[textureName])
                }
        
                window[textureName].image.src = "textures/"+textureName+".png";
        }


/**
 * Place la planète à son orbit avec sa rotation et son tilt.
 * @return {void}
 */
this.place = function(){
    mat4.rotate(mvMatrix, (rotation*(30/(this.orbrot/50))), [0,1,0]);
    mat4.translate(mvMatrix, [this.orbit, 0, -0]);
    mat4.rotate(mvMatrix, (3.14*this.tilt), [0,0,1]);

    mat4.rotate(mvMatrix, -(rotation*1000), [0,1,0]);
    }

/**
 * Sert à réinitialise la position de la matrice après avoir dessiner la planète
 * @return {void}
 */
this.unplace = function(){
    mat4.rotate(mvMatrix, (rotation*1000), [0,1,0]);
    mat4.rotate(mvMatrix, -(3.14*this.tilt), [0,0,1]);
    mat4.translate(mvMatrix, [-(this.orbit), 0, -0]);
    mat4.rotate(mvMatrix, -(rotation*(30/(this.orbrot/50))), [0,1,0]);
    }
}







/**
 * Définit un carré en 2D. La structure est la même que la classe sphère.
 * @param  {float} size        Définit la taille du carré. Corresponds à la taille des côtés.
 * @param  {String} textureName Définit le nom du fichier texture à utiliser. Le fichier doit être de type ".png" et être placé dans un sous-dossier textures
 * @return {void}
 */
var plane = function(size,textureName){
    this.sizeC=size;

    this.cubeVertexPositionBuffer;
    this.cubeVertexNormalBuffer;
    this.cubeVertexTextureCoordBuffer;
    this.cubeVertexIndexBuffer;
    window[textureName];

     this.buffer= function(){
        this.cubeVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexPositionBuffer);
        vertices = [
            // Front face
            -(this.sizeC/2),  0.0, -(this.sizeC/2),
             (this.sizeC/2),  0.0, -(this.sizeC/2),
             (this.sizeC/2),  0.0,  (this.sizeC/2),
            -(this.sizeC/2),  0.0,  (this.sizeC/2),
        ];

        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        this.cubeVertexPositionBuffer.itemSize = 3;
        this.cubeVertexPositionBuffer.numItems = 4;

        this.cubeVertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexNormalBuffer);
        var vertexNormals = [
            // Front face
             0.0,  0.0,  1.0,
             0.0,  0.0,  1.0,
             0.0,  0.0,  1.0,
             0.0,  0.0,  1.0,

            // Back face


            // Left face
    
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexNormals), gl.STATIC_DRAW);
        this.cubeVertexNormalBuffer.itemSize = 3;
        this.cubeVertexNormalBuffer.numItems = 4;

        this.cubeVertexTextureCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexTextureCoordBuffer);
        var textureCoords = [
            // Front face
            0.0, 0.0,
            1.0, 0.0,
            1.0, 1.0,
            0.0, 1.0,



            // Left face
  
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
        this.cubeVertexTextureCoordBuffer.itemSize = 2;
        this.cubeVertexTextureCoordBuffer.numItems = 4;

        this.cubeVertexIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeVertexIndexBuffer);
        var cubeVertexIndices = [
            0, 1, 2,      0, 2, 3,    // Front face
         
           
        ];
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
        this.cubeVertexIndexBuffer.itemSize = 1;
        this.cubeVertexIndexBuffer.numItems = 6;
     }


this.bind = function(){
    gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexPositionBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexNormalBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.cubeVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeVertexTextureCoordBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, window[textureName]);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeVertexIndexBuffer);
}

this.draw = function(){
    gl.drawElements(gl.TRIANGLES, this.cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
}

this.createTexture = function(){
        window[textureName] = gl.createTexture();
                window[textureName].image = new Image();
                window[textureName].image.onload = function () {
                    handleLoadedTexture(window[textureName])
                }
        
                window[textureName].image.src = "textures/"+textureName+".png";
        }
}







/**
 * circle : Sers à dessiner les cercles pour les orbits des planètes. La structure est la même que la classe sphère.
 * @param  {float} size     rayon du cercle à dessiner.
 * @return {void}
 */
var circle = function(size){

    this.sizeC=size;

    this.circleVertexPositionBuffer;
    this.circleVertexNormalBuffer;
    this.circleVertexColorBuffer;
    this.circleVertexIndexBuffer;
    this.circleVertexIndices;

    var unpackedColors;
    var vectices;
    var normalData;


    this.buffer= function(){
        var n = 60
        var twoPi = 2.0 * 3.14159;

        vertices = [0, 0, 0];
        normalData = [0, 0, 1];
        unpackedColors = [1, 0, 0, 1];
        this.circleVertexIndices = [];

        for (var j = 0; j < n; j++) {
            vertices = vertices.concat([this.sizeC*Math.cos((j-1) * twoPi / n), 0.0 ,this.sizeC*Math.sin((j-1) * twoPi / n)]);
            normalData = normalData.concat([0, 0, 1]);
            unpackedColors = unpackedColors.concat([1.0, 0.0, 0.0, 1.0]);
            this.circleVertexIndices = this.circleVertexIndices.concat([j+1,(j+1<n?j+2:1)]);
        }

        console.log(this.circleVertexIndices);

        this.circleVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexPositionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        this.circleVertexPositionBuffer.itemSize = 3;
        this.circleVertexPositionBuffer.numItems = vertices.length / this.circleVertexPositionBuffer.itemSize;

        this.circleVertexColorBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexColorBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(unpackedColors), gl.STATIC_DRAW);
        this.circleVertexColorBuffer.itemSize = 4;
        this.circleVertexColorBuffer.numItems = unpackedColors.length / this.circleVertexColorBuffer.itemSize;

        this.circleVertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexNormalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normalData), gl.STATIC_DRAW);
        this.circleVertexNormalBuffer.itemSize = 3;
        this.circleVertexNormalBuffer.numItems = normalData.length / this.circleVertexNormalBuffer.itemSize;

        this.circleVertexIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.circleVertexIndexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.circleVertexIndices), gl.STATIC_DRAW);
        this.circleVertexIndexBuffer.itemSize = 1;
        this.circleVertexIndexBuffer.numItems = this.circleVertexIndices.length / this.circleVertexIndexBuffer.itemSize;
    }


    this.bind = function(){

       gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexPositionBuffer);
       gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.circleVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

       gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexColorBuffer);
       gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.circleVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);

       gl.bindBuffer(gl.ARRAY_BUFFER, this.circleVertexNormalBuffer);
       gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, this.circleVertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

       gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.circleVertexIndexBuffer);
       setMatrixUniforms();

   }

   this.draw = function(){
    gl.drawElements(gl.LINE_STRIP, this.circleVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
}
}
